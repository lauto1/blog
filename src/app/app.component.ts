import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {



	constructor() {
        // Initialize Firebase
        var config = {
          apiKey: "AIzaSyDFcDnuPtfNiF5LZrEVedB_4F_vuhiTwpU",
          authDomain: "blog-3343f.firebaseapp.com",
          databaseURL: "https://blog-3343f.firebaseio.com",
          projectId: "blog-3343f",
          storageBucket: "blog-3343f.appspot.com",
          messagingSenderId: "553039287749"
        };
        firebase.initializeApp(config);
  }
  

}




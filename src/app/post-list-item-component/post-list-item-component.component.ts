import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { PostsListService } from '../services/posts-list.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import * as firebase from 'firebase';
import { Post } from '../services/postModel';




@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})

export class PostListItemComponentComponent implements OnInit , OnDestroy{
  isAuth = this.authService.isAuth;
  post: Post ; 
  @Input() id:string;
  @Input() postIndex:number;
  @Input() title :string ;
  @Input() image: string;
  @Input() content :string ;
  @Input() loveIts :number =0;
  @Input() created_at :string ;
  subscription : Subscription;

  constructor(private postsListService:PostsListService, private authService : AuthService) {
  
   }

   ngOnInit(){
    firebase.auth().onAuthStateChanged(
      (user)=>{
        if(user){
          this.isAuth = true;
        }else{
          this.isAuth = false;
        }
      }
    );

    this.subscription = this.postsListService.postSubject.subscribe(
      (data)=>this.post = data
    );
  //  this.postsListService.getPostById(this.id);
   }

  onLoveIt(event:any){
    this.loveIts ++;
    let patch = { loveIts: this.loveIts }
    this.postsListService.updatePost(this.id, patch);
    console.log(this.loveIts);
  }

  onDontLoveIt(event:any){
    this.loveIts --;
    let patch = { loveIts: this.loveIts }
    this.postsListService.updatePost(this.id, patch);
    console.log(this.loveIts);
  }
  
  getColor(){
    if(this.loveIts<0){
      return '#FF9999';
    }else if(this.loveIts>0){
      return 'lightgreen';
    }else if (this.loveIts=== 0){
      return 'lightgrey';
    }
  }
  

  delPost(){
    this.postsListService.deletePost(this.id);
    this.postsListService.emitPostsSubject();
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}




import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsListService } from '../services/posts-list.service';
import { Post } from '../services/postModel';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit, OnDestroy {
  isAuth=this.authService.isAuth;
  post: any;
  id: string;
  titre: string;
  image: string;
  content: string;
  created_at;
  loveIts: number;
  subsciption: Subscription;

  constructor(private route: ActivatedRoute,private router : Router, private postsList: PostsListService,private authService:AuthService) { }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(
      (user)=>{
        if(user){
          this.isAuth = true;
        }else{
          this.isAuth = false;
        }
      }
    )
    this.id = this.route.snapshot.params['id'];
    this.subsciption = this.postsList.postSubject.subscribe(
      (data) => {
        this.post = data;
        this.titre = this.post.titre;
        this.image = this.post.image;
        this.content = this.post.content;
        this.created_at = this.post.created_at;
        this.loveIts = data.loveIts;
        this.postsList.emitPostSubject()
      },
      (err) => console.error(err),
      () => console.log('success')
      
    )
    this.postsList.getPostById(this.id);

  }

  onLoveIt(event: any) {
    this.loveIts++;
    let patch = { loveIts: this.loveIts }
    this.postsList.updatePost(this.id, patch);
  }

  onDontLoveIt(event: any) {
    this.loveIts--;
    let patch = { loveIts: this.loveIts };
    this.postsList.updatePost(this.id, patch);
  }
  getColor() {
    if (this.loveIts < 0) {
      return '#FF9999';
    } else if (this.loveIts > 0) {
      return 'lightgreen';
    } else if (this.loveIts === 0) {
      return 'lightgrey';
    }
  }


  delPost() {
    this.postsList.deletePost(this.id);
    this.router.navigate(['']);
  }
  
  ngOnDestroy() {
    this.subsciption.unsubscribe();
  }
}

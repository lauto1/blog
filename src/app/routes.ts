import { Routes } from '@angular/router';
import { PostListComponentComponent } from './post-list-component/post-list-component.component';
import { NewPostComponent } from './new-post/new-post.component';
import { SinglePostComponent } from './single-post/single-post.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { AuthGuard } from './auth.guard';

export const appRoutes: Routes = [
    { path: '',  component: PostListComponentComponent },
    {path:'register', component:SignUpComponent},
    {path:'login',component:LoginComponent},
    {path:'profil',canActivate:[AuthGuard],component:ProfilComponent},
    { path: 'new', canActivate:[AuthGuard],component: NewPostComponent },
    {path: 'posts',component: PostListComponentComponent},
    {path:'posts/:id',component: SinglePostComponent},
    {path:'not-found',component:PostListComponentComponent},
    {path:'**',redirectTo:'not-found'}
  ];
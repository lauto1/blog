export class Post {
    id:string;
    titre: string;
    image:string;
    content: string;
    created_at: any = new Date();
    loveIts:number = 0;
  
  
    constructor(values:Object={}) {
      Object.assign(this,values)
    }
   
  }
  

import { Injectable } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Post } from './postModel';
import { Subject } from 'rxjs';
import { map } from '@firebase/util';
import { single } from 'rxjs/operators';
import { from } from 'rxjs/observable/from';

const url: string = "https://blog-3343f.firebaseio.com/";
@Injectable()
export class PostsListService {
  private posts: Post[] = [];
  private post:Post;
  postsSubject = new Subject<Post[]>();
  postSubject = new Subject<Post>();

  constructor(private router: Router, private http: HttpClient) { }




  getPosts() {
    this.http.get<Post[]>(url + 'posts.json').subscribe(
      data => {
        let posts = [];
        for (let [id, post] of Object.entries(data)) {
          post.id = id;
          post.created_at = Date.parse(post.created_at);
          posts.push(post);
        }
        this.posts = posts;
        this.emitPostsSubject();
      }
    )
  }
  
  emitPostsSubject() {
    this.postsSubject.next(this.posts);
  }
  emitPostSubject(){
    this.postSubject.next(this.post)
  }
  getPostById(id){ console.log(id)
    this.http.get<Post>(url+'posts/'+id+'.json').subscribe(
      (response)=>{
        this.post = response;
        this.post.id = id ;
        this.post.created_at = Date.parse(this.post.created_at);
       
        this.emitPostSubject();
        this.emitPostsSubject();
      },
      (err)=>console.error(err),
      ()=>console.info()
    )
  }

  addPost(post) {
    this.http.post<{name}>(url + 'posts.json', post).subscribe(
      (data) => {
        post.id = data.name;
        this.posts.push(post);
        this.emitPostsSubject();
      },
      (err) => console.error(err),
      () => console.log('success')
    )
    
  }
  updatePost(id,value){
    this.http.patch<Post>(url+'posts/'+id+'.json',value).subscribe(
      (data)=>{console.log(data);this.emitPostSubject();
        this.emitPostsSubject();},
      (err)=>console.error(err),
      ()=>{}
    )
    
  }
  deleteAllPosts(){
    this.http.delete<Post>(url + 'posts.json').subscribe(
      (data)=>{console.log(data);this.emitPostsSubject();},
      (err)=>console.error(err),
      ()=>console.log('all Posts have been delete')
    )
    
  }
  findPost(id){
    let source = from(this.posts);
    source.pipe(single(val=>val.id===id)).subscribe(
      (data)=>{this.post=data;console.log(data,this.post);}
    )
  }
  deletePost(id){
    this.http.delete<{name}>(url+'posts/'+id+'.json').subscribe(
      (data)=>{
        this.findPost(id);
        let idx = this.posts.indexOf(this.post);
        this.posts.splice(idx,1);

        console.log(data,this.post,this.posts);
      },
      (err)=>console.error(err),
      ()=>console.log('success')
    )
    this.emitPostsSubject();
  }

}

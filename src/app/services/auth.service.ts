import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

@Injectable()
export class AuthService {
  isAuth:boolean = false;
  
  constructor() { }

  createUser(email:string, password:string){
    return new Promise(function(resolve,reject){
      return firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email,password).then(
        function(){
          return resolve();
        },function(error){
          return reject(error);
        }
      );
    });
  }

  connectUser(email:string,password:string){
    return new Promise(
      function(resolve,reject){
        return firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email,password).then(
          function(){
            console.log(firebase.auth.Auth)
            return resolve();
            
          },
          function(error){
            return reject(error);

          }
        );
      }
    )
  }
  disconnectUser(){
    firebase.auth().signOut();
  }

}

import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { User } from './UserModel';
import * as firebase from 'firebase';

@Injectable()
export class UsersListService {
  usersList: AngularFireList<any>;
  selectedUser: User = new User();

  constructor(private firebase: AngularFireDatabase) { }

  //CRUD

  createUser(user: User) {
    this.usersList.push({
      name: user.name,
      email: user.email,
      password:user.password,
      avatar: user.avatar
    });
  }

  getUsers(){
    this.usersList = this.firebase.list('users');
    return this.usersList;
  }

  updateUser(user : User){
    this.usersList.update(user.$key,{
      name : user.name,
      email:user.email,
      password : user.password,
      avatar : user.avatar
    });
  }
  deleteUser($key:string){
    this.usersList.remove($key);
  }
}



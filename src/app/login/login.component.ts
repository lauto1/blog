import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm : FormGroup;

  constructor(private authService : AuthService , private router : Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email : new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    })
  }

  onSubmit(loginForm){
    this.authService.connectUser(this.loginForm.value.email , this.loginForm.value.password);
    this.router.navigate(['/posts']);
  }
}

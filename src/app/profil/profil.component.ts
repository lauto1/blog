import { Component, OnInit } from '@angular/core';
import { User } from '../services/UserModel';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { PostsListService } from '../services/posts-list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  profilForm: FormGroup;
  oldName: string='aaaaaaaaaa';
  oldEmail: string="aaaaaaaa";
  oldAvatar: string="https://media.giphy.com/media/kHVyqoTuOT96A60zOQ/giphy.gif";
  users: any;

  constructor(private authService: AuthService, private postsListService : PostsListService, private router: Router) {

  }

  ngOnInit() {
    this.profilForm = new FormGroup({
      name: new FormControl(''),
      email: new FormControl('', Validators.pattern('/[0-9a-zA-Z]{6,}/')),
      avatar: new FormControl('')
    });

  }
  onSubmit(loginForm){

  }
  deleteAll(){
    this.postsListService.deleteAllPosts();
    this.router.navigate(['']);
  }
}

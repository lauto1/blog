import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { PostsListService } from '../services/posts-list.service';
import { Subscription } from 'rxjs';
import { Post } from '../services/postModel';



@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit, OnDestroy {
 posts:Post[];
 subscription: Subscription;

  constructor(private postsListService:PostsListService) { 

  }

  ngOnInit() {
    this.postsListService.getPosts();
    
   this.subscription = this.postsListService.postsSubject.subscribe(
     (data)=>{this.posts=data;
      },
     (err)=>console.error(err),
     ()=>console.log('success')
   );
   
    
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Post } from '../services/postModel';
import { PostsListService } from '../services/posts-list.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  post: Post;
  postForm: FormGroup;
  titre = new FormControl('Post', Validators.required);
  image = new FormControl('image');
  content = new FormControl('content', Validators.required);
  created_at = new Date();

  constructor(private postList: PostsListService, private routes: Router) {

  }

  ngOnInit() {
    this.postForm = new FormGroup({ titre: new FormControl('', Validators.required), image: new FormControl(''), content: new FormControl('', Validators.required) });
  }

  onSubmit(postForm) {
    this.post = new Post(this.postForm.value);
    this.postList.addPost(this.post);
    this.routes.navigate(['']);
  }

}

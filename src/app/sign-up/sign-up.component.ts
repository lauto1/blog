import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as Mat from 'materialize-css';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  constructor(private authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.email, Validators.required]),
      passwords: new FormGroup({
        password: new FormControl('', [Validators.required, Validators.minLength(8)]),
        confirmPwd: new FormControl('', [Validators.required, Validators.minLength(8)])
      }),
      avatar : new FormControl('',Validators.required)
    });
  }

  onSubmit(signUpForm) {
    if (signUpForm.value.passwords.password === signUpForm.value.passwords.confirmPwd) {
      this.authService.createUser(signUpForm.value.email,signUpForm.value.passwords.password);
      this.router.navigate(['/posts']);
    } else {
      this.passwordsError();
    }

  }

  passwordsError(){
    Mat.toast({html: "Passwords don't match"});
  }
}

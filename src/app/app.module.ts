import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, LOCALE_ID } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { PostListComponentComponent } from './post-list-component/post-list-component.component';
import { PostListItemComponentComponent } from './post-list-item-component/post-list-item-component.component';
import { NewPostComponent } from './new-post/new-post.component';
import { RouterModule } from '@angular/router';
import { PostsListService } from './services/posts-list.service';
import { appRoutes } from './routes';
import { SinglePostComponent } from './single-post/single-post.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './services/auth.service';
import * as firebase from 'firebase';
import { ProfilComponent } from './profil/profil.component';
import { UsersListService } from './services/users-list.service';
import { HeaderComponent } from './header/header.component';
import { AuthGuard } from './auth.guard';
import { EditPostComponent } from './edit-post/edit-post.component';





@NgModule({
  declarations: [
    AppComponent,
    PostListComponentComponent,
    PostListItemComponentComponent,
    NewPostComponent,
    SinglePostComponent,
    SignUpComponent,
    LoginComponent,
    ProfilComponent,
    HeaderComponent,
    EditPostComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  providers: [
    PostsListService,
    AuthService,
    AuthGuard,
    UsersListService,
    {provide: LOCALE_ID,useValue: 'FR'}
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }


import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import * as firebase from 'firebase';
import * as M from 'materialize-css/dist/js/materialize';
import { element } from 'protractor';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
isAuth : boolean = this.authService.isAuth;

  constructor(private authService : AuthService) {  }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(
      (user)=>{
        if(user){
          this.isAuth = true;
        }else{
          this.isAuth = false;
        }
      }
    );
    $(document).ready(function(){
      $('.sidenav').sidenav();
    });
  }
  clickMenu(){
    let element = document.querySelector('.sidenav');
    var instance = M.Sidenav.getInstance(element);
    instance.open();
  }
  closeMenu(){
    let element = document.querySelector('.sidenav');
    var instance = M.Sidenav.getInstance(element);
    instance.close();
  }
  onSignOut(){
    this.authService.disconnectUser();
  }
}

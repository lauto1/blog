import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(function(resolve,reject){
      firebase.auth().onAuthStateChanged(
        (user)=>{
          if(user){
            resolve(true);
          }else{
            this.router.navigate(['/login','register']);
            resolve(false);
          }
        }
      )
    });
  }
}
